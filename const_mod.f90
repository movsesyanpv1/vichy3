module const
	integer, parameter :: mp = 8
	integer, parameter :: n = 3
	real(mp), parameter :: eps = 1e-6
	real(mp), parameter :: a = 1.0
	real(mp), parameter :: d = 1.0
	real(mp), parameter :: e = 1.0
end module const
