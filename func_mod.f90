module func
    use const
    implicit none
contains

    function lambda(ma)

        real(mp), dimension(n,n) :: ma
        real(mp), dimension(n) :: xw,x,x1
        real(mp), dimension(n) :: lambda

        x1 = 0
        x1(1) = 1
        xw = 10

        do while(sqrt(dot_product(x1-x,x1-x)).ge.eps)
            x = x1
            xw = matmul(ma,x)
            x1 = xw/xw(1)
        enddo

        lambda = xw

    end function lambda

    subroutine lambdarev(ma,x, mu,t0,mE)

        real(mp), dimension(n,n) :: mal,ma,mE
        real(mp), dimension(n) :: xw,x,x1
        real(mp), dimension(n+1,n) :: man
        real(mp) :: mu,t0,t

        x1 = 0
        x1(1) = 1
        xw = 10
        t=t0
        do while(sqrt(dot_product(x1-x,x1-x)).ge.eps)
            mal = ma - t*mE
            x1 = x
            xw=jordan_scheme(mal,x1)
            mu = xw(1)
            t = t + 1.0/mu
            x = xw/mu
        enddo
        t0 = t

    end subroutine lambdarev

    subroutine lambdarev0(ma,x, mu,t0,mE)

        real(mp), dimension(n,n) :: mal,ma,mE
        real(mp), dimension(n) :: xw,x,x1
        real(mp), dimension(n+1,n) :: man
        real(mp) :: mu,t0,t

        x1 = 0
        x1(1) = 1
        xw = 10
        t=t0
        do while(sqrt(dot_product(x1-x,x1-x)).ge.eps)
            mal = ma - t*mE
            x1 = x
            xw=jordan_scheme(mal,x1)
            mu = xw(1)
            t = t + 1.0/mu
            x = xw/mu
        enddo
        t0 = t

    end subroutine lambdarev0

    function ort(a,b,j)

        real(mp), dimension(:,:) :: a
        real(mp), dimension(n) :: b
        real(mp), dimension(n) :: ort
        integer :: i,j

        ort = b
        do i = 1, j
            ort = ort - proj(a(i,:),b)
        enddo

    end function ort

    function proj(a,b)

        real(mp), dimension(n) :: a, b
        real(mp), dimension(n) :: proj

        proj = dot_product(a,b) / dot_product(a,a) * a

    end function proj

function jordan_scheme(A,B) result(x)                               ! Метод Гаусса, схема Жордана.
    real(mp) A(:,:), B(:)
    real(mp) x(1:size(B)), aa(1:size(B),1:size(B)+1)
    integer n,i,j,k
    
    n=size(B)
    aa(1:n,1:n)=A
    aa(1:n,n+1)=B
    do k=1,n                                                        ! Грядёт длинное условие проверки ведущего элемента на малость
        if (abs(aa(k,k))<=10_mp**(-2*mp+2)) write(*,*) 'Warning: leading element is close to 0'
        aa(k,:)=aa(k,:)/aa(k,k)
        forall(i=1:n, j=k:n+1, i/=k) aa(i,j)=aa(i,j)-aa(k,j)*aa(i,k)! Матрица полность диагонализуется,
    enddo
    x=aa(:,n+1)                                                     ! в результате решение просто совпадает с пребразованным вектором B.
end

        function maxlocmy(a1,l,n)result(o)                                  !Поиск индексов максимального элемента.

        implicit none

        real(mp), dimension(n+1,n) :: a1
        integer, dimension(2)      :: o
        integer                    :: l,n,i1,j1
        real(mp)                   ::m

        m = abs(a1(l,l))
        o = (/l,l/)

        do i1 = l, n
            do j1 = l, n
                if((abs(a1(j1,i1)) > m) .and. (a1(j1,i1) /= 0))then               
                    m = abs(a1(j1,i1))
                    o = (/j1,i1/)
                endif
            enddo
        enddo

        end function maxlocmy

end module func
