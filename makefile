run: comp
	./prog; rm *.o *.mod prog
comp: module prog.f90
	gfortran prog.f90 *_mod.o -o prog
module: *_mod.f90
	gfortran -c *mod.f90
